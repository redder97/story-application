import { Component, OnInit } from '@angular/core';
import { ActionHandlerService } from '../action-handler.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  process(componentName){
    this.actionHandler.redirect(componentName);
  }

  constructor(private actionHandler : ActionHandlerService) { }

  ngOnInit() {
  }

}
