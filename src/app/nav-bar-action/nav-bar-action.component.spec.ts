import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarActionComponent } from './nav-bar-action.component';

describe('NavBarActionComponent', () => {
  let component: NavBarActionComponent;
  let fixture: ComponentFixture<NavBarActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBarActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
