import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav-bar-action',
  templateUrl: './nav-bar-action.component.html',
  styleUrls: ['./nav-bar-action.component.css']
})
export class NavBarActionComponent implements OnInit {

  @Input() name : string;

  constructor() { }

  ngOnInit() {
  }

}
