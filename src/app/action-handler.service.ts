import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ActionHandlerService {

  redirect(actionName : string) : void{

    if(actionName == "Home"){
      
    }else if(actionName == "Profile"){
      this.router.navigate(['/profile'])
    }
  }

  constructor(private router : Router) { }
}
